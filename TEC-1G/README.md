# Embedded Vezza

This is a very cut down "Embedded" version of [Vezza](https://gitlab.com/sijnstra1/vezza), my z80 based Z-Machine/Infocom interpreter

This version is built for the TEC-1G, expanded with GLCD and Qwerty keyboard. February 2025 update includes the use of the additional 16k available under EXPAND.
As the binary is integrated with the game, the .HEX files include individual games.

![Catseye screenshot](TEC-1G_vezza_Catseye.jpg "Catseye running on the TEC-1G")

Main features supported:
* Play a variety of zmachine version games
* Handcrafted highly optimised code to support best gameplay experience
* Support mapping accented characters to something displayable
* Text highlighting where possible
* Updated to take advantage of the extra 16k of memory available using EXPAND

Feature limitations compared to previous versions:
* Game file included with binary in a single file
* No save, restore, restart, verify
* No transcript
* No timed input
* Game file currently limited to around ~~26k~~42k

# Provided Games
The following .HEX files are provided as a combined Embedded-VEZZA with a game. Once the .HEX file is loaded on the TEC-1G, you'll need to jump to address 0x2000 to run the game.

`e-vezza_DT.hex` The Dragon and the Troll <br>
by Johan Berntsson _Gamefile in .z5 format_

`e-vezza_catseye.hex` CATSEYE <br>
Miniventure #2 by Dave Bernazzani _Gamefile in .z3 format_

`e-vezza_wumpus.hex` Hunt the Wumpus <br>
Inform port by Magnus Olsson _Gamefile in .z5 format_

`e-vezza_ADVland_part1.hex` `e-vezza_ADVland_part2.hex` Adventureland <br>
Inform 6 port of the Scott Adams classic _Adventureland_
This is an early version of the port, which fits in a mere 42k of game file.
Instructions for loading:
* Use the `settings` function to `Toggle EXPAND`. The EXPAND LED status LED should now be lit up
* Use the `Intel Hex Load` function to load _Part 2_
* Use the `settings` function to `Toggle EXPAND`. The EXPAND LED status LED should now be off
* Use the `Intel Hex Load` function to load _Part 1_
* Now you can use the address selection button to execute from `2000`h

# Known Issues
* There are some cosmetic issues with entering very input long lines.
* Display is slow to render.
* Other issues are known which are in the pipeline to fix and enable further features.

# Writing Small Games
The smallest game library I'm aware of for z-machine inform games is minform by Dave Bernazzani, however, I don't have a live link to it.

# Roadmap
There's a few extra features to be built and other games to be included. Stay tuned!
