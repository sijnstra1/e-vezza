# Embedded Vezza

This is a very cut down "Embedded" version of [Vezza](https://gitlab.com/sijnstra1/vezza), my z80 based Z-Machine/Infocom interpreter

This version is built for the tape based Spectrum machines.
Some files are built for the 48k Spectrum, some for the 128k Spectrum.
As the binary is integrated with the game, the .TAP files included

Main features supported:
* Play a variety of zmachine version games
* Handcrafted highly optimised code to support best gameplay experience
* Support mapping accented characters to something displayable
* Text highlighting where possible

Feature limitations compared to previous versions:
* Game file included with binary in a single file
* No save, restore, restart, verify
* No transcript
* No timed input
* On a 128k Machine - Game file limited to 96k
* On a 48k Machine - Game file limited to 29k
* On a 48k Machine - Stack size limited to 1k


# Provided Games
The following .TAP files are provided as a combined Embedded-VEZZA with a game (48k minimum):

`e-vezza_moonglow.tap` MOONGLOW <br>
Miniventure #1 by Dave Bernazzani _Gamefile in .z3 format_

`e-vezza_catseye.tap` CATSEYE <br>
Miniventure #2 by Dave Bernazzani _Gamefile in .z3 format_

`e-vezza_zombies.tap` Zombies! <br>
by Chris Cenotti  _Gamefile in .z3 format_

`e-vezza_wumpus.tap` Hunt the Wumpus <br>
Inform port by Magnus Olsson _Gamefile in .z5 format_

**128k required** for the following:

`e-vezza_zork1_128k_beta.tap` Zork 1 <br>
Zork 1 (Release 30) - Classic Infocom adventure by Marc Blank and Dave Lebling

`e-vezza_zork2_128k_beta.tap` Zork 2 <br>
Zork 2 - Classic Infocom adventure by Dave Lebling, Marc Blank

`e-vezza_zork3_128k_beta.tap` Zork 3 <br>
Zork 2 - Classic Infocom adventure by Dave Lebling, Marc Blank

`e-vezza_infidel_128k_beta.tap` Infidel <br>
Infidel - Classic Infocom adventure by Michael Berlyn

`e-vezza_starcross_128k_beta.tap` Starcross <br>
Starcross - Classic Infocom adventure by Dave Lebling

`e-vezza_balances_128k_beta.tap` Balances <br>
Balances - A small unofficial sequel to Infocom's Enchanter trilogy by Graham Nelson using .z5

# Known Issues
There are some cosmetic issues with entering very input long lines.

# Writing Small Games
The smallest game library I'm aware of for z-machine inform games is minform by Dave Bernazzani, however, I don't have a live link to it.

# Acknowledgements
These releases made possible by the public availability of the game files. <br>
The 128k releases were made possibe by Tom Dalby's ZXSTLC loader https://tomdalby.com/other/zxstlc.html
