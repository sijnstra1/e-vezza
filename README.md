# Embedded Vezza

This is a very cut down "Embedded" version of [Vezza](https://gitlab.com/sijnstra1/vezza), my z80 based Z-Machine/Infocom interpreter. The full featured version is supported on a range of platforms from the TRS-80 Models 1,3 and 4 through the Amstrad CPC, MSX, Agon Light and many more.

The same hand crafted highly optimised z-machine engine is used; the source files are common across all of the platforms for the core.

The versions here are built for systems that do not have large random access storage, hence the description "embedded".
Embedded systems supported so far are:
* ZX [Spectrum](https://gitlab.com/sijnstra1/e-vezza/-/tree/main/Spectrum) (in both 48k and 128k versions)
* [TEC-1G](https://gitlab.com/sijnstra1/e-vezza/-/tree/main/TEC-1G) (requires fully populated RAM, MON3, GLCD, QWERTY keyboard)

Main features supported:
* Play a variety of zmachine version games
* Handcrafted highly optimised code to support best gameplay experience
* Support mapping accented characters to something displayable
* Text highlighting where possible

Feature limitations compared to previous versions:
* Game file included with binary in a single file
* No save, restore, restart, verify
* No transcript
* No timed input

Note that the binary still support z1-z5, z7 and z8 games although I would be surprised if a z7 or z8 game turned out to be useful.

# Known Issues
There are some cosmetic issues with entering very input long lines.

# Writing Small Games
The smallest game library I'm aware of for z-machine inform games is minform by Dave Bernazzani, however, I don't have a live link to it. If you know where this game library can be found, please share.
